#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

extern void freeze(void);
extern void unfreeze(void);
extern void reset_counters(void);
extern void stop_jack(void);

void
on_freeze_toggled                      (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  if (gtk_toggle_button_get_active(togglebutton)) {
    freeze();
  } else {
    unfreeze();
  }
}


void
on_reset_clicked                       (GtkButton       *button,
                                        gpointer         user_data)
{
  reset_counters();
}


gboolean
window_delete                          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  freeze();
  return FALSE;
}

