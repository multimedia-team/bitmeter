#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <jack/jack.h>

#define ALL_BITS (256 + 24)

extern void integer_bit(int bit, int color);
extern void sign_bit(int color);
extern void mantissa_bit(int bit, int color);
extern void update_freq(unsigned int freq);
extern void update_oops(unsigned int nan, unsigned int inf,
                        unsigned int denormal);
extern void update_range(float min_db, float max_db);

jack_client_t *jack_handle;
jack_port_t *in_port;
char client_name[]= "bitmeter-XXXXXXXXXX";

static void ieee_stats(float *ieee);

static int signal_count;

void sigterm_exit(int sig)
{
   signal_count++;
   if (signal_count < 2) {
     printf("Closing JACK client.\n");
     jack_client_close (jack_handle);
   } else {
     printf("JACK client closed.\n");
   }
   exit(0);
}

int playback_callback(jack_nframes_t nframes, void *arg)
{
  int k;

  jack_default_audio_sample_t *in = (jack_default_audio_sample_t *)jack_port_get_buffer(in_port, nframes);  

  for (k= 0; k < nframes; ++k) {
    ieee_stats(in + k);
  }

  return(0);
}

jack_client_t *open_jack(void)
{
  jack_client_t *jack_handle;
            
  sprintf(client_name, "bitmeter-%d", getpid());
  if ((jack_handle = jack_client_new(client_name)) == 0) {
    fprintf (stderr, "jack server not running ?\n");
    exit (1);
  }
  in_port = jack_port_register(jack_handle, "in", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput | JackPortIsTerminal, 0);
  jack_set_process_callback(jack_handle, playback_callback, 0);
  return(jack_handle);
}

static unsigned int set[23];
static unsigned int one[ALL_BITS], hit[ALL_BITS];
static unsigned int samples = 0; /* count of samples */
static unsigned int positive = 0; /* of which, +ve */

static float min_db = 999.9f, max_db = -999.9f;
static unsigned long nan_hit = 0, inf_hit = 0, zero_hit = 0, denormal_hit = 0;

static void
ieee_stats(float *ieee)
{
  unsigned int *ref = (unsigned int *) ieee, value = *ref;
  unsigned int exp = (value & 0x7f800000) >> 23;
  int sign = (value & 0x80000000) ? -1 : 1;
  int k;

  value &= 0x7fffff;

  samples++;
  if (exp == 255) {
    if (value == 0) inf_hit++; else nan_hit++;
    return;
  } else if (exp == 0 && value == 0) {
    zero_hit++;
    return;
  } else if (exp == 0) {
    denormal_hit++;
  }

  if (sign > 0) positive++;

  if (exp > 0) {
    float db = 20.0f * log10f(fabsf(*ieee));
    if (max_db < db) max_db = db;
    if (min_db > db) min_db = db;
    /* implicit one */
    hit[exp + 23]++;
    one[exp + 23]++;
  } else {
    exp = 1; /* E-126 not E-127 for denormals */
  }
  
  for (k= 0; k < 23; ++k) {
    const int bit = 1 << k;
    hit[exp + k]++;
    if (value & bit) {
      set[k]++;
      one[exp + k]++;
    }
  }
}

static void
finish_ponder()
{
  int k;

  update_range(min_db, max_db);
  update_oops(nan_hit, inf_hit, denormal_hit);

  if (samples == zero_hit) {
    sign_bit(0);
  } else if (positive == 0) {
    sign_bit(1);
  } else if (positive == samples) {
    sign_bit(5);
  } else if (3 * positive > 2 * samples) {
    sign_bit(4);
  } else if (3 * positive < samples) {
    sign_bit(2);
  } else {
    sign_bit(3);
  }

  for (k= 0; k < 23; ++k) {
    if (samples == zero_hit) {
      mantissa_bit(k, 0);
    } else if (set[k] == 0) {
      mantissa_bit(k, 1);
    } else if (set[k] == samples) {
      mantissa_bit(k, 5);
    } else if (3 * set[k] > 2 * samples) {
      mantissa_bit(k, 4);
    } else if (3 * set[k] < samples) {
      mantissa_bit(k, 2);
    } else {
      mantissa_bit(k, 3);
    }
  }

  for (k= 0; k < 40; ++k) {
    const int offset = k + 118;
    if (hit[offset] == 0) {
      integer_bit(k, 0);
    } else if (one[offset] == 0) {
      integer_bit(k, 1);
    } else if (one[offset] == samples) {
      integer_bit(k, 5);
    } else if (3 * one[offset] > 2 * samples) {
      integer_bit(k, 4);
    } else if (3 * one[offset] < samples) {
      integer_bit(k, 2);
    } else {
      integer_bit(k, 3);
    }
  }

  zero_hit = positive = samples = 0;
  max_db = -999.9f; min_db = 999.9f;

  for (k= 0; k < 23; ++k) {
    set[k] = 0;
  }
  for (k= 0; k < ALL_BITS; ++k) {
    one[k] = 0;
    hit[k] = 0;
  }
}

void
reset_counters(void)
{
  nan_hit = inf_hit = denormal_hit = 0;
}

int
start_jack (int argc, char *argv[])
{
  jack_handle = open_jack();

  if (jack_activate(jack_handle)) {
    fprintf(stderr, "cannot activate client");
    exit(1);
  }

  if (argc > 1)
    jack_connect(jack_handle, argv[1], jack_port_name(in_port));

  update_freq(jack_get_sample_rate(jack_handle));
  return TRUE;
}

int
run_jack (void *ignore)
{
  finish_ponder();
  return TRUE;
}

void
stop_jack()
{
  jack_client_close (jack_handle);
}
