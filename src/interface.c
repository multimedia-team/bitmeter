/*
 * DO NOT EDIT THIS FILE - it is generated by Glade.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#define GLADE_HOOKUP_OBJECT(component,widget,name) \
  g_object_set_data_full (G_OBJECT (component), name, \
    gtk_widget_ref (widget), (GDestroyNotify) gtk_widget_unref)

#define GLADE_HOOKUP_OBJECT_NO_REF(component,widget,name) \
  g_object_set_data (G_OBJECT (component), name, widget)

GtkWidget*
create_bitmeter_main (void)
{
  GtkWidget *bitmeter_main;
  GdkPixbuf *bitmeter_main_icon_pixbuf;
  GtkWidget *vbox1;
  GtkWidget *table1;
  GtkWidget *freq_label;
  GtkWidget *min_label;
  GtkWidget *inf_label;
  GtkWidget *denormal_label;
  GtkWidget *max_label;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label6;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *nan_label;
  GtkWidget *hseparator1;
  GtkWidget *hbox1;
  GtkWidget *signbit;
  GtkWidget *vseparator1;
  GtkWidget *mantissa22;
  GtkWidget *mantissa21;
  GtkWidget *mantissa20;
  GtkWidget *mantissa19;
  GtkWidget *mantissa18;
  GtkWidget *mantissa17;
  GtkWidget *mantissa16;
  GtkWidget *mantissa15;
  GtkWidget *mantissa14;
  GtkWidget *mantissa13;
  GtkWidget *mantissa12;
  GtkWidget *mantissa11;
  GtkWidget *mantissa10;
  GtkWidget *mantissa09;
  GtkWidget *mantissa08;
  GtkWidget *mantissa07;
  GtkWidget *mantissa06;
  GtkWidget *mantissa05;
  GtkWidget *mantissa04;
  GtkWidget *mantissa03;
  GtkWidget *mantissa02;
  GtkWidget *mantissa01;
  GtkWidget *mantissa00;
  GtkWidget *hbox2;
  GtkWidget *integer39;
  GtkWidget *integer38;
  GtkWidget *integer37;
  GtkWidget *integer36;
  GtkWidget *integer35;
  GtkWidget *integer34;
  GtkWidget *integer33;
  GtkWidget *integer32;
  GtkWidget *vseparator2;
  GtkWidget *integer31;
  GtkWidget *integer30;
  GtkWidget *integer29;
  GtkWidget *integer28;
  GtkWidget *integer27;
  GtkWidget *integer26;
  GtkWidget *integer25;
  GtkWidget *integer24;
  GtkWidget *integer23;
  GtkWidget *integer22;
  GtkWidget *integer21;
  GtkWidget *integer20;
  GtkWidget *integer19;
  GtkWidget *integer18;
  GtkWidget *integer17;
  GtkWidget *integer16;
  GtkWidget *integer15;
  GtkWidget *integer14;
  GtkWidget *integer13;
  GtkWidget *integer12;
  GtkWidget *integer11;
  GtkWidget *integer10;
  GtkWidget *integer09;
  GtkWidget *integer08;
  GtkWidget *integer07;
  GtkWidget *integer06;
  GtkWidget *integer05;
  GtkWidget *integer04;
  GtkWidget *integer03;
  GtkWidget *integer02;
  GtkWidget *integer01;
  GtkWidget *integer00;
  GtkWidget *hbox5;
  GtkWidget *table2;
  GtkWidget *freeze;
  GtkWidget *hbox6;
  GtkWidget *label9;
  GtkWidget *reset;
  GtkWidget *hbox7;
  GtkWidget *label10;

  bitmeter_main = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (bitmeter_main, "bitmeter_main");
  gtk_window_set_title (GTK_WINDOW (bitmeter_main), _("JACK Bitmeter"));
  gtk_window_set_resizable (GTK_WINDOW (bitmeter_main), FALSE);
  bitmeter_main_icon_pixbuf = create_pixbuf ("bitmeter.xpm");
  if (bitmeter_main_icon_pixbuf)
    {
      gtk_window_set_icon (GTK_WINDOW (bitmeter_main), bitmeter_main_icon_pixbuf);
      gdk_pixbuf_unref (bitmeter_main_icon_pixbuf);
    }

  vbox1 = gtk_vbox_new (FALSE, 6);
  gtk_widget_set_name (vbox1, "vbox1");
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (bitmeter_main), vbox1);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 12);

  table1 = gtk_table_new (3, 5, FALSE);
  gtk_widget_set_name (table1, "table1");
  gtk_widget_show (table1);
  gtk_box_pack_start (GTK_BOX (vbox1), table1, FALSE, FALSE, 0);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 12);

  freq_label = gtk_label_new (_("48000 Hz"));
  gtk_widget_set_name (freq_label, "freq_label");
  gtk_widget_show (freq_label);
  gtk_table_attach (GTK_TABLE (table1), freq_label, 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (freq_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (freq_label), GTK_JUSTIFY_RIGHT);
  gtk_label_set_selectable (GTK_LABEL (freq_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (freq_label), 1, 0.5);

  min_label = gtk_label_new (_("0.0 dB"));
  gtk_widget_set_name (min_label, "min_label");
  gtk_widget_show (min_label);
  gtk_table_attach (GTK_TABLE (table1), min_label, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (min_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (min_label), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (min_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (min_label), 1, 0.5);

  inf_label = gtk_label_new (_("0"));
  gtk_widget_set_name (inf_label, "inf_label");
  gtk_widget_show (inf_label);
  gtk_table_attach (GTK_TABLE (table1), inf_label, 4, 5, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (inf_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (inf_label), GTK_JUSTIFY_RIGHT);
  gtk_label_set_selectable (GTK_LABEL (inf_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (inf_label), 1, 0.5);

  denormal_label = gtk_label_new (_("0"));
  gtk_widget_set_name (denormal_label, "denormal_label");
  gtk_widget_show (denormal_label);
  gtk_table_attach (GTK_TABLE (table1), denormal_label, 4, 5, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (denormal_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (denormal_label), GTK_JUSTIFY_RIGHT);
  gtk_label_set_selectable (GTK_LABEL (denormal_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (denormal_label), 1, 0.5);

  max_label = gtk_label_new (_("-12.0 dB"));
  gtk_widget_set_name (max_label, "max_label");
  gtk_widget_show (max_label);
  gtk_table_attach (GTK_TABLE (table1), max_label, 2, 3, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (max_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (max_label), GTK_JUSTIFY_RIGHT);
  gtk_label_set_selectable (GTK_LABEL (max_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (max_label), 1, 0.5);

  label1 = gtk_label_new (_("Sample Rate"));
  gtk_widget_set_name (label1, "label1");
  gtk_widget_show (label1);
  gtk_table_attach (GTK_TABLE (table1), label1, 0, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label1, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label1), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label1), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label1), 0, 0.5);

  label2 = gtk_label_new (_("Smallest non-zero sample"));
  gtk_widget_set_name (label2, "label2");
  gtk_widget_show (label2);
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label2, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label2), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label2), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label2), 0, 0.5);

  label3 = gtk_label_new (_("Largest sample"));
  gtk_widget_set_name (label3, "label3");
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label3, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label3), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label3), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label3), 0, 0.5);

  label6 = gtk_label_new (_("Denormal"));
  gtk_widget_set_name (label6, "label6");
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 3, 4, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label6, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label6), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label6), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label6), 0, 0.5);

  label4 = gtk_label_new (_("Not a Number"));
  gtk_widget_set_name (label4, "label4");
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label4, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label4), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label4), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label4), 0, 0.5);

  label5 = gtk_label_new (_("Infinite"));
  gtk_widget_set_name (label5, "label5");
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 3, 4, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (label5, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_LEFT);
  gtk_label_set_selectable (GTK_LABEL (label5), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label5), 0, 0.5);

  nan_label = gtk_label_new (_("0"));
  gtk_widget_set_name (nan_label, "nan_label");
  gtk_widget_show (nan_label);
  gtk_table_attach (GTK_TABLE (table1), nan_label, 4, 5, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  GTK_WIDGET_SET_FLAGS (nan_label, GTK_CAN_FOCUS);
  gtk_label_set_justify (GTK_LABEL (nan_label), GTK_JUSTIFY_RIGHT);
  gtk_label_set_selectable (GTK_LABEL (nan_label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (nan_label), 1, 0.5);

  hseparator1 = gtk_hseparator_new ();
  gtk_widget_set_name (hseparator1, "hseparator1");
  gtk_widget_show (hseparator1);
  gtk_box_pack_start (GTK_BOX (vbox1), hseparator1, TRUE, FALSE, 0);

  hbox1 = gtk_hbox_new (FALSE, 1);
  gtk_widget_set_name (hbox1, "hbox1");
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, FALSE, 0);

  signbit = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (signbit, "signbit");
  gtk_widget_show (signbit);
  gtk_box_pack_start (GTK_BOX (hbox1), signbit, FALSE, FALSE, 0);

  vseparator1 = gtk_vseparator_new ();
  gtk_widget_set_name (vseparator1, "vseparator1");
  gtk_widget_show (vseparator1);
  gtk_box_pack_start (GTK_BOX (hbox1), vseparator1, FALSE, FALSE, 2);

  mantissa22 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa22, "mantissa22");
  gtk_widget_show (mantissa22);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa22, FALSE, FALSE, 0);

  mantissa21 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa21, "mantissa21");
  gtk_widget_show (mantissa21);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa21, FALSE, FALSE, 0);

  mantissa20 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa20, "mantissa20");
  gtk_widget_show (mantissa20);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa20, FALSE, FALSE, 0);

  mantissa19 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa19, "mantissa19");
  gtk_widget_show (mantissa19);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa19, FALSE, FALSE, 0);

  mantissa18 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa18, "mantissa18");
  gtk_widget_show (mantissa18);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa18, FALSE, FALSE, 0);

  mantissa17 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa17, "mantissa17");
  gtk_widget_show (mantissa17);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa17, FALSE, FALSE, 0);

  mantissa16 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa16, "mantissa16");
  gtk_widget_show (mantissa16);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa16, FALSE, FALSE, 0);

  mantissa15 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa15, "mantissa15");
  gtk_widget_show (mantissa15);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa15, FALSE, FALSE, 0);

  mantissa14 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa14, "mantissa14");
  gtk_widget_show (mantissa14);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa14, FALSE, FALSE, 0);

  mantissa13 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa13, "mantissa13");
  gtk_widget_show (mantissa13);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa13, FALSE, FALSE, 0);

  mantissa12 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa12, "mantissa12");
  gtk_widget_show (mantissa12);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa12, FALSE, FALSE, 0);

  mantissa11 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa11, "mantissa11");
  gtk_widget_show (mantissa11);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa11, FALSE, FALSE, 0);

  mantissa10 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa10, "mantissa10");
  gtk_widget_show (mantissa10);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa10, FALSE, FALSE, 0);

  mantissa09 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa09, "mantissa09");
  gtk_widget_show (mantissa09);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa09, FALSE, FALSE, 0);

  mantissa08 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa08, "mantissa08");
  gtk_widget_show (mantissa08);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa08, FALSE, FALSE, 0);

  mantissa07 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa07, "mantissa07");
  gtk_widget_show (mantissa07);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa07, FALSE, FALSE, 0);

  mantissa06 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa06, "mantissa06");
  gtk_widget_show (mantissa06);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa06, FALSE, FALSE, 0);

  mantissa05 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa05, "mantissa05");
  gtk_widget_show (mantissa05);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa05, FALSE, FALSE, 0);

  mantissa04 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa04, "mantissa04");
  gtk_widget_show (mantissa04);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa04, FALSE, FALSE, 0);

  mantissa03 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa03, "mantissa03");
  gtk_widget_show (mantissa03);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa03, FALSE, FALSE, 0);

  mantissa02 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa02, "mantissa02");
  gtk_widget_show (mantissa02);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa02, FALSE, FALSE, 0);

  mantissa01 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa01, "mantissa01");
  gtk_widget_show (mantissa01);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa01, FALSE, FALSE, 0);

  mantissa00 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (mantissa00, "mantissa00");
  gtk_widget_show (mantissa00);
  gtk_box_pack_start (GTK_BOX (hbox1), mantissa00, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 1);
  gtk_widget_set_name (hbox2, "hbox2");
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox2, FALSE, FALSE, 0);

  integer39 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer39, "integer39");
  gtk_widget_show (integer39);
  gtk_box_pack_start (GTK_BOX (hbox2), integer39, FALSE, FALSE, 0);

  integer38 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer38, "integer38");
  gtk_widget_show (integer38);
  gtk_box_pack_start (GTK_BOX (hbox2), integer38, FALSE, FALSE, 0);

  integer37 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer37, "integer37");
  gtk_widget_show (integer37);
  gtk_box_pack_start (GTK_BOX (hbox2), integer37, FALSE, FALSE, 0);

  integer36 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer36, "integer36");
  gtk_widget_show (integer36);
  gtk_box_pack_start (GTK_BOX (hbox2), integer36, FALSE, FALSE, 0);

  integer35 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer35, "integer35");
  gtk_widget_show (integer35);
  gtk_box_pack_start (GTK_BOX (hbox2), integer35, FALSE, FALSE, 0);

  integer34 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer34, "integer34");
  gtk_widget_show (integer34);
  gtk_box_pack_start (GTK_BOX (hbox2), integer34, FALSE, FALSE, 0);

  integer33 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer33, "integer33");
  gtk_widget_show (integer33);
  gtk_box_pack_start (GTK_BOX (hbox2), integer33, FALSE, FALSE, 0);

  integer32 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer32, "integer32");
  gtk_widget_show (integer32);
  gtk_box_pack_start (GTK_BOX (hbox2), integer32, FALSE, FALSE, 0);

  vseparator2 = gtk_vseparator_new ();
  gtk_widget_set_name (vseparator2, "vseparator2");
  gtk_widget_show (vseparator2);
  gtk_box_pack_start (GTK_BOX (hbox2), vseparator2, FALSE, FALSE, 2);

  integer31 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer31, "integer31");
  gtk_widget_show (integer31);
  gtk_box_pack_start (GTK_BOX (hbox2), integer31, FALSE, FALSE, 0);

  integer30 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer30, "integer30");
  gtk_widget_show (integer30);
  gtk_box_pack_start (GTK_BOX (hbox2), integer30, FALSE, FALSE, 0);

  integer29 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer29, "integer29");
  gtk_widget_show (integer29);
  gtk_box_pack_start (GTK_BOX (hbox2), integer29, FALSE, FALSE, 0);

  integer28 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer28, "integer28");
  gtk_widget_show (integer28);
  gtk_box_pack_start (GTK_BOX (hbox2), integer28, FALSE, FALSE, 0);

  integer27 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer27, "integer27");
  gtk_widget_show (integer27);
  gtk_box_pack_start (GTK_BOX (hbox2), integer27, FALSE, FALSE, 0);

  integer26 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer26, "integer26");
  gtk_widget_show (integer26);
  gtk_box_pack_start (GTK_BOX (hbox2), integer26, FALSE, FALSE, 0);

  integer25 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer25, "integer25");
  gtk_widget_show (integer25);
  gtk_box_pack_start (GTK_BOX (hbox2), integer25, FALSE, FALSE, 0);

  integer24 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer24, "integer24");
  gtk_widget_show (integer24);
  gtk_box_pack_start (GTK_BOX (hbox2), integer24, FALSE, FALSE, 0);

  integer23 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer23, "integer23");
  gtk_widget_show (integer23);
  gtk_box_pack_start (GTK_BOX (hbox2), integer23, FALSE, FALSE, 0);

  integer22 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer22, "integer22");
  gtk_widget_show (integer22);
  gtk_box_pack_start (GTK_BOX (hbox2), integer22, FALSE, FALSE, 0);

  integer21 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer21, "integer21");
  gtk_widget_show (integer21);
  gtk_box_pack_start (GTK_BOX (hbox2), integer21, FALSE, FALSE, 0);

  integer20 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer20, "integer20");
  gtk_widget_show (integer20);
  gtk_box_pack_start (GTK_BOX (hbox2), integer20, FALSE, FALSE, 0);

  integer19 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer19, "integer19");
  gtk_widget_show (integer19);
  gtk_box_pack_start (GTK_BOX (hbox2), integer19, FALSE, FALSE, 0);

  integer18 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer18, "integer18");
  gtk_widget_show (integer18);
  gtk_box_pack_start (GTK_BOX (hbox2), integer18, FALSE, FALSE, 0);

  integer17 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer17, "integer17");
  gtk_widget_show (integer17);
  gtk_box_pack_start (GTK_BOX (hbox2), integer17, FALSE, FALSE, 0);

  integer16 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer16, "integer16");
  gtk_widget_show (integer16);
  gtk_box_pack_start (GTK_BOX (hbox2), integer16, FALSE, FALSE, 0);

  integer15 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer15, "integer15");
  gtk_widget_show (integer15);
  gtk_box_pack_start (GTK_BOX (hbox2), integer15, FALSE, FALSE, 0);

  integer14 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer14, "integer14");
  gtk_widget_show (integer14);
  gtk_box_pack_start (GTK_BOX (hbox2), integer14, FALSE, FALSE, 0);

  integer13 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer13, "integer13");
  gtk_widget_show (integer13);
  gtk_box_pack_start (GTK_BOX (hbox2), integer13, FALSE, FALSE, 0);

  integer12 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer12, "integer12");
  gtk_widget_show (integer12);
  gtk_box_pack_start (GTK_BOX (hbox2), integer12, FALSE, FALSE, 0);

  integer11 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer11, "integer11");
  gtk_widget_show (integer11);
  gtk_box_pack_start (GTK_BOX (hbox2), integer11, FALSE, FALSE, 0);

  integer10 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer10, "integer10");
  gtk_widget_show (integer10);
  gtk_box_pack_start (GTK_BOX (hbox2), integer10, FALSE, FALSE, 0);

  integer09 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer09, "integer09");
  gtk_widget_show (integer09);
  gtk_box_pack_start (GTK_BOX (hbox2), integer09, FALSE, FALSE, 0);

  integer08 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer08, "integer08");
  gtk_widget_show (integer08);
  gtk_box_pack_start (GTK_BOX (hbox2), integer08, FALSE, FALSE, 0);

  integer07 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer07, "integer07");
  gtk_widget_show (integer07);
  gtk_box_pack_start (GTK_BOX (hbox2), integer07, FALSE, FALSE, 0);

  integer06 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer06, "integer06");
  gtk_widget_show (integer06);
  gtk_box_pack_start (GTK_BOX (hbox2), integer06, FALSE, FALSE, 0);

  integer05 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer05, "integer05");
  gtk_widget_show (integer05);
  gtk_box_pack_start (GTK_BOX (hbox2), integer05, FALSE, FALSE, 0);

  integer04 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer04, "integer04");
  gtk_widget_show (integer04);
  gtk_box_pack_start (GTK_BOX (hbox2), integer04, FALSE, FALSE, 0);

  integer03 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer03, "integer03");
  gtk_widget_show (integer03);
  gtk_box_pack_start (GTK_BOX (hbox2), integer03, FALSE, FALSE, 0);

  integer02 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer02, "integer02");
  gtk_widget_show (integer02);
  gtk_box_pack_start (GTK_BOX (hbox2), integer02, FALSE, FALSE, 0);

  integer01 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer01, "integer01");
  gtk_widget_show (integer01);
  gtk_box_pack_start (GTK_BOX (hbox2), integer01, FALSE, FALSE, 0);

  integer00 = create_pixmap (bitmeter_main, NULL);
  gtk_widget_set_name (integer00, "integer00");
  gtk_widget_show (integer00);
  gtk_box_pack_start (GTK_BOX (hbox2), integer00, FALSE, FALSE, 0);

  hbox5 = gtk_hbox_new (FALSE, 6);
  gtk_widget_set_name (hbox5, "hbox5");
  gtk_widget_show (hbox5);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox5, TRUE, TRUE, 0);

  table2 = gtk_table_new (1, 2, TRUE);
  gtk_widget_set_name (table2, "table2");
  gtk_widget_show (table2);
  gtk_box_pack_end (GTK_BOX (hbox5), table2, FALSE, FALSE, 0);
  gtk_table_set_col_spacings (GTK_TABLE (table2), 6);

  freeze = gtk_toggle_button_new ();
  gtk_widget_set_name (freeze, "freeze");
  gtk_widget_show (freeze);
  gtk_table_attach (GTK_TABLE (table2), freeze, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

  hbox6 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox6, "hbox6");
  gtk_widget_show (hbox6);
  gtk_container_add (GTK_CONTAINER (freeze), hbox6);

  label9 = gtk_label_new_with_mnemonic (_("_Freeze"));
  gtk_widget_set_name (label9, "label9");
  gtk_widget_show (label9);
  gtk_box_pack_start (GTK_BOX (hbox6), label9, TRUE, TRUE, 0);
  gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_LEFT);
  gtk_misc_set_padding (GTK_MISC (label9), 12, 0);

  reset = gtk_button_new ();
  gtk_widget_set_name (reset, "reset");
  gtk_widget_show (reset);
  gtk_table_attach (GTK_TABLE (table2), reset, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
  GTK_WIDGET_SET_FLAGS (reset, GTK_CAN_DEFAULT);

  hbox7 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox7, "hbox7");
  gtk_widget_show (hbox7);
  gtk_container_add (GTK_CONTAINER (reset), hbox7);

  label10 = gtk_label_new_with_mnemonic (_("_Reset"));
  gtk_widget_set_name (label10, "label10");
  gtk_widget_show (label10);
  gtk_box_pack_start (GTK_BOX (hbox7), label10, TRUE, TRUE, 0);
  gtk_label_set_justify (GTK_LABEL (label10), GTK_JUSTIFY_LEFT);
  gtk_misc_set_padding (GTK_MISC (label10), 12, 0);

  g_signal_connect ((gpointer) bitmeter_main, "delete_event",
                    G_CALLBACK (window_delete),
                    NULL);
  g_signal_connect ((gpointer) bitmeter_main, "delete_event",
                    G_CALLBACK (gtk_main_quit),
                    NULL);
  g_signal_connect ((gpointer) freeze, "toggled",
                    G_CALLBACK (on_freeze_toggled),
                    NULL);
  g_signal_connect ((gpointer) reset, "clicked",
                    G_CALLBACK (on_reset_clicked),
                    NULL);

  /* Store pointers to all widgets, for use by lookup_widget(). */
  GLADE_HOOKUP_OBJECT_NO_REF (bitmeter_main, bitmeter_main, "bitmeter_main");
  GLADE_HOOKUP_OBJECT (bitmeter_main, vbox1, "vbox1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, table1, "table1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, freq_label, "freq_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, min_label, "min_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, inf_label, "inf_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, denormal_label, "denormal_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, max_label, "max_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label1, "label1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label2, "label2");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label3, "label3");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label6, "label6");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label4, "label4");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label5, "label5");
  GLADE_HOOKUP_OBJECT (bitmeter_main, nan_label, "nan_label");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hseparator1, "hseparator1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hbox1, "hbox1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, signbit, "signbit");
  GLADE_HOOKUP_OBJECT (bitmeter_main, vseparator1, "vseparator1");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa22, "mantissa22");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa21, "mantissa21");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa20, "mantissa20");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa19, "mantissa19");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa18, "mantissa18");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa17, "mantissa17");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa16, "mantissa16");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa15, "mantissa15");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa14, "mantissa14");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa13, "mantissa13");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa12, "mantissa12");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa11, "mantissa11");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa10, "mantissa10");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa09, "mantissa09");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa08, "mantissa08");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa07, "mantissa07");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa06, "mantissa06");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa05, "mantissa05");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa04, "mantissa04");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa03, "mantissa03");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa02, "mantissa02");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa01, "mantissa01");
  GLADE_HOOKUP_OBJECT (bitmeter_main, mantissa00, "mantissa00");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hbox2, "hbox2");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer39, "integer39");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer38, "integer38");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer37, "integer37");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer36, "integer36");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer35, "integer35");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer34, "integer34");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer33, "integer33");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer32, "integer32");
  GLADE_HOOKUP_OBJECT (bitmeter_main, vseparator2, "vseparator2");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer31, "integer31");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer30, "integer30");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer29, "integer29");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer28, "integer28");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer27, "integer27");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer26, "integer26");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer25, "integer25");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer24, "integer24");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer23, "integer23");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer22, "integer22");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer21, "integer21");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer20, "integer20");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer19, "integer19");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer18, "integer18");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer17, "integer17");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer16, "integer16");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer15, "integer15");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer14, "integer14");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer13, "integer13");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer12, "integer12");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer11, "integer11");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer10, "integer10");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer09, "integer09");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer08, "integer08");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer07, "integer07");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer06, "integer06");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer05, "integer05");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer04, "integer04");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer03, "integer03");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer02, "integer02");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer01, "integer01");
  GLADE_HOOKUP_OBJECT (bitmeter_main, integer00, "integer00");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hbox5, "hbox5");
  GLADE_HOOKUP_OBJECT (bitmeter_main, table2, "table2");
  GLADE_HOOKUP_OBJECT (bitmeter_main, freeze, "freeze");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hbox6, "hbox6");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label9, "label9");
  GLADE_HOOKUP_OBJECT (bitmeter_main, reset, "reset");
  GLADE_HOOKUP_OBJECT (bitmeter_main, hbox7, "hbox7");
  GLADE_HOOKUP_OBJECT (bitmeter_main, label10, "label10");

  return bitmeter_main;
}

