#include <gtk/gtk.h>


void
on_freeze_pressed                      (GtkButton       *button,
                                        gpointer         user_data);

void
on_reset_activate                      (GtkButton       *button,
                                        gpointer         user_data);

void
on_freeze_toggled                      (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_reset_clicked                       (GtkButton       *button,
                                        gpointer         user_data);

gboolean
window_delete                          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);
