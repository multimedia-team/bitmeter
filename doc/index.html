<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
 <title>JACK bitmeter documentation</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rev=made href="mailto:njl98r@ecs.soton.ac.uk">
 <link rel=toc href="/~njl98r/code/">
 <link rel=stylesheet type="text/css" href="/~njl98r/base.css">
</head>
<body>

<h1>JACK bitmeter</h1>

<p>JACK bitmeter is a diagnosis tool for JACK audio software on Linux
(and perhaps other systems which have JACK and GTK+ 2.x). As its name
might suggest, the bitmeter operates at the bare metal of JACK's I/O
layer, looking at the 32 binary digits in each individual sample.</p>

<p>Think of bitmeter like the cable detector in your toolbox. You cannot
use a cable detector to drive screws into wood, or to undo bolts, or measure
the length of a copper pipe, but it's the perfect tool for detecting hidden
cables in walls before you drill. You will rarely need bitmeter, in fact
most JACK users will never use it at all, but having it and never needing
it is much better than needing it and not having it.</p>

<h2>Using the bitmeter</h2>

<p>Each instance of bitmeter has a single JACK input, and is uniquely
numbered. Usually you'll want to connect exactly one JACK output to the
bitmeter, as the results can otherwise be rather confusing.</p>

<div><img src="jack.png" alt=""></div>

<p>In the above diagram one bitmeter has been attached to an ALSA PCM
output (in this case sampled from a microphone) and another to the
2nd channel of a running AlsaPlayer. On the command line you can ask
a bitmeter to connect itself to an existing output by specifying the
full JACK name (such as <code>MyApp:output_4</code>) of the port.</p>

<p>You can “freeze” the display at any time using the labelled button,
this doesn't affect the collection of statistics, but it does let you
keep an anomaly on the screen long enough to show someone else or get a
screenshot. Click once (the button stays pushed in) to freeze, and again
to unfreeze. You can also reset the count of irregular samples, which
is normally cumulative.</p>

<h2>Interpreting the display</h2>

<p>There are three main areas to the bitmeter display.</p>
<ul><li>Statistics, at the top, including the range of sample values and
keeping a count of irregular / illegal sample values such as NaN. The
statistics on the right are cumulative, and should ordinarily read zero.
</li><li>Sign & Mantissa, a row of 24 coloured indicators showing the
sign (positive or negative) and mantissa of the samples.
</li><li>Adjusted scale, 40 smaller coloured indicators explained later.
</li></ul>

<p>The sign and mantissa statistics are show as coloured indicators which
map to bits in the samples processed by the bitmeter, with the left most
indicator representing the sign bit, and then the mantissa left to right
from most significant to least significant. The colour is based on the
percentage of samples in which the associated bit was 1 over a period of
100ms or so. Blue indicates that all samples were 0, a light green-blue for
up to 33%, green for 33-66% (ie about half), and orange for more than 66%,
then finally red if all samples are 1, a possible “stuck bit”. Gray is
used when no samples touched the associated bit.</p>

<p>The “adjusted scale” shows each sample bit on a absolute scale, adjusted
for the exponent of the sample, so that internally the bitmeter records
a 280-bit binary real. For simplicity only 40 bits are displayed, the
8 left-most bits are the integer part, and the remaining 32 bits after the
marker are fractional bits.</p>

<p>The audio range of the adjusted scale is from about 200dB below FS
to 40dB above, which would be excessive for audio work but proves useful
in diagnosing problems at a lower level.</p>

<p>The sample rate reported by bitmeter is directly from JACK. It's not
used to perform any calculations and is purely informative.</p>

<h3>Zero</h3>
<div><img src="zero.png" alt=""></div>

<p>Absolute silence, also seen when no JACK connection has been made.</p>
<p>Both sets of coloured indicators are entirely gray, so there were
no valid non-zero samples. The smallest / largest sample readings show
'NA'.</p>

<h3>Quiet, 16-bit samples</h3>
<div><img src="16quiet.png" alt=""></div>

<p>This is from a quiet piece of music recorded through a 16-bit card,
the same thing would be seen with a sample from a CD.</p>
<p>The statistics show that the smallest non-zero sample was at about
-90dB, an indication that the source data is 16-bit integer PCM.
The largest sample was well below full scale.</p>

<p>The sign indicator is green, meaning that samples were more or less
evenly distributed between positive & negative. The topmost (leftmost)
four bits of the mantissa are green, then a further three are green-blue
and the remainder are blue. At this low amplitude only 8 bits of
sample accuracy is present in the integer PCM signal.</p>

<p>The adjusted scale shows that all bits below 2<sup><small>-15</small></sup>
were zero, which again indicates 16-bit PCM.</p>

<h3>Loud, 16-bit samples</h3>
<div><img src="16full.png" alt=""></div>

<p>Here the amplitude of the input signal has increased significantly,
in fact it's probably clipping slightly. The statistics now show 0dB
full scale samples, and the top 14 bits of the mantissa are all green.</p>
<p>On the adjusted scale the '1-bit' is green-blue, as we would expect
if there were some full-scale samples.</p>

<h3>Quiet, 24-bit samples</h3>
<div><img src="24quiet.png" alt=""></div>

<p>This is from a quiet passage in a recording made with a 24-bit ADC.
You should expect the same thing with 24-bit external AES/EBU or S/PDIF
audio, for example from off-board digital effects units.</p>

<p>This time the smallest non-zero sample was about -123dB and the largest
sample was more than 40dB below full scale. We can see there is more audio
content in this signal than in even the full scale 16-bit example given
earlier, because more bits are used in the mantissa.</p>

<p>On the adjusted scale it's clear that this is 24-bit audio because bits
zero to eight inclusive are always blue (zero). For 16-bit audio it would
be bits zero through sixteen counting from the right.</p>

<h3>Loud, 24-bit samples</h3>
<div><img src="24full.png" alt=""></div>

<p>Here the display has been frozen (the button appears pressed in) when
the signal began clipping at 0dB. The adjusted scale correctly suggests
that this is also a 24-bit audio input, and one can see that even with
the full range of 24-bit audio the last bit of the floating point mantissa
is unused.</p>

<h3>31-bit white noise</h3>
<div><img src="31noise.png" alt=""></div>

<p>This sample was created using a white noise sample generated with a
random number generator equivalent to 31 bits of integer precision.
The rightmost two bits of the adjusted scale are blue (zero), indicating
30 bits of precision after the sign bit.</p>

<h3>Random samples</h3>
<div><img src="random.png" alt=""></div>

<p>In contrast to the previous example, this time the sample values were
evenly distributed over a broad range of values in the IEEE floating
point representation, yet still random. This produces a very different
sound from white noise, and a distinctive pattern in the adjusted scale, as
well as this remarkable -758dB minimum non-zero value.</p>

<p>The adjusted scale shows green-blue all the way across 32 bits,
a much more even spread than associated with normal signals.</p>

<h3>Irregular samples</h3>
<h4>Not a Number</h4>
<div><img src="NaN.png" alt=""></div>

<p>Some arithmetic operations on floating point numbers have no defined
answer in the natural numbers. For example the square root of a negative
number. This result has no meaning as a PCM sample value, and should be
discarded. You can see the associated irregular sample counter is
increasing in this screenshot because NaNs are arriving at the input of
the bitmeter.</p>

<h4>Infinite</h4>
<div><img src="infinite.png" alt=""></div>

<p>Another class of operations results in answers that are too large to
be represented, or are altogether too large to be natural numbers at all.
The delicate algorithms used in filters may “explode” if exposed to an
infinity, ceasing to produce useful output even when the input returns
to a more sensible value. Again the appropriate counter is increasing.</p>

<h4>Denormal</h4>
<div><img src="denormal.png" alt=""></div>

<p>Samples too small to be represented in an ordinary JACK 32-bit float
but not small enough to be zero are denormals. Obviously too small to be
significant in playback, denormals are a problem if naive signal processing
algorithms attempt to do arithmetic operations on them. Such operations
are very slow on denormals and can cause a plug-in or application to exceed
its allowable processing time and be kicked from the JACK graph.</p>

<p>JACK applications are expected both to protect themselves against
irregular samples and to prevent such samples from being emitted. A suitable
test, named JACK demolition in reference to the LADSPA demolition tool, is
available from Steve Harris. The above three examples were created using
JACK demolition.</p>

<h3>Mistake #1</h3>
<h4>Division by 32767</h4>
<div><img src="32767.png" alt=""></div>

<p>Although JACK itself works entirely with IEEE floating point values
the conversion to and from analog audio uses integers, as do popular
audio storage technologies like DAT and Red Book CDs. For correct
operation JACK software which uses such integers should use the same
conversion ratios as JACK itself. e.g. 16-bit samples should be
divided by exactly 32768.</p>

<p>A common mistake is to choose the value 32767 instead. You can't hear
this, or see it with ordinary meters, but the bitmeter shows a clear
signature for audio processed in this way. The 8th bit of the mantissa
(counting the rightmost as the 0th) is orange, indicating that an
unusually high percentage of samples have this bit set.</p>

</body>
</html>
